# Hummus

## Zutaten

* 240g Kichererbsen, gekocht
* 80 ml Aqua Faba (ca. die Hälfte des Abtropfwassers bei Kichererbsen aus der Dose)
* 1-2 Knoblauchzehen
* 1 Zitrone, ausgepresst
* ½ TL Meersalz
* ½ TL Kreuzkümmel
* 80 g Tahin

#### Zum Garnieren

* 1-2 EL Olivenöl, zum Garnieren
* ¼ TL Paprikapulver, Rosenscharf oder Chilipulver

## Zubereitung

1. Kichererbsen abtropfen, das Abtropfwasser dabei auffangen
   1. Benötigte Menge des Kichererbsenwassers zu Schnee schlagen.
   2. Abgetropfte Kichererbsen fein wolfen.
2. Zitronensaft und Knoblauch fein pürieren.
3. Tahin hinzufügen und gut durchmixen.
4. Kichererbsenschnee, gewolfte Kichererbsen, Salz und Kreuzkümmel untermischen.
5. Vor dem Servieren mit Olivenöl und Paprika / Chili garnieren.

## Inspiration

[https://www.schuesselglueck.de/perfekten-hummus-selber-machen/](https://www.schuesselglueck.de/perfekten-hummus-selber-machen/)
