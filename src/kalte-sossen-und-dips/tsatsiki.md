# Tsatsiki

## Zutaten

* 150 g pflanzlicher Hirtenkäse
* 1 Gurke, grob gerieben
* 1 Zitrone, ausgepresst
* 1 Knoblauchzehe
* ½ TL Meersalz
* ½ TL Pfeffer, frisch gemahlen
* 1 EL Olivenöl

## Zubereitung

1. Gurke waschen und reiben. Die Gurkenraspeln mit dem Salz vermischen und min. 1 Stunde abtropfen lassen, das Gurkenwasser dabei auffangen.
2. Hirtenkäse würfeln und mit dem Zitronensaft und der Hälfte des Gurkenwassers glatt pürieren. Nach und nach die andere Hälfte des Gurkenwassers hinzugeben, bis eine cremige Konsistenz erreicht ist.&#x20;
3. Olivenöl und Gurkenraspeln unterrühren. Mit frisch gemahlenem Pfeffer abschmecken.
