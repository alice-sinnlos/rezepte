# Summary

[Willkommen](./README.md)

# Rezepte

- [Kalte Soßen & Dips]()
  - [Hummus](kalte-sossen-und-dips/hummus.md)
  - [Tsatsiki](kalte-sossen-und-dips/tsatsiki.md)
- [Brot & Brötchen]()
  - [Burger Buns](brot-und-broetchen/burger-buns.md)
- [Gebratenes & Frittiertes]()
  - [Burger Patties](gebratenes-und-frittiertes/burger-patties.md)
  - [Falafel]()
- [Kartoffel & Getreide]()
  - [Gnocchi](kartoffel-getreide/gnocchi.md)
  - [Polenta]()
- [Hülsenfrüchte]()
  - [Masoor Dal](huelsenfruechte/masoor-dal.md)
- [Süßspeisen]()
  - [Pfannkuchen](suessspeisen/pfannkuchen.md)
