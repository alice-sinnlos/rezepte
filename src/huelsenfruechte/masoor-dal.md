# Masoor Dal (Rotes Dal)

## Zutaten

* 150 g Rote Linsen
* 150 g Möhren
* 1 große Zwiebel
* 1-2 Knoblauchzehen
* 1 EL Ingwer, frisch, fein gewürfelt
* 1 Chili, fein geschnitten
* 1-2 EL Kokosfett *oder* Pflanzenöl
* 360 ml Gemüsebrühe
* 200 ml Kokosmilch
* 1 EL Tomatenmark
* 1 EL Süßungsmittel (z.B. Dattelsirup)
* 1½ TL Kreuzkümmel
* 1 EL Curry Pulver
* 1 TL Kurkuma
* 1 TL Paprika, edelsüß
* ½ TL Meersalz
* Prise schwarzer Pfeffer, frisch gemahlen

#### Zum Garnieren

* Pflanzlicher Joghurt
* Koriandergrün, fein geschnitten

#### Optional

* 250 g Kartoffeln, vorwiegend festkochend
* 200 g frischer Blattspinat

## Zubereitung

1. Die Kartoffel in Würfel Schneiden (ca. 1,5 cm Kantenlänge) und in dem Fett in einer nicht haftenden Pfanne goldbraun anbraten.
2. Währenddessen Karotte und Zwiebel würfeln, mitbraten.
3. Dann Ingwer, Chili und Knoblauch fein schneiden und ebenfalls kurz mit anbraten, währenddessen die Gewürze hinzufügen.
4. Die Linsen in einen Topf geben, mit der Brühe aufgießen und zum Kochen bringen.
5. Die angebratenen Gemüse, das Tomatenmark und das Süßungsmittel hinzufügen und ca. 10 Minuten köcheln lassen, bis die Linsen weich sind, aber noch Biss haben.
6. Kokosmilch (und ggf. Spinat) hinzufügen, kurz aufkochen, mit Salz und Pfeffer abschmecken.
7. Mit Joghurt und Koriander garnieren.

## Inspiration

[elavegan.com/de/rotes-linsen-dal](https://elavegan.com/de/rotes-linsen-dal/)
