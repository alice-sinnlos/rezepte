---
description: 8 Stk. á 125g
---

# Burger Patties

## Zutaten

* 100 g Soja-Schnetzel
* 100 g braune Linsen (am Vortag eingeweicht in 250 ml Wasser)
* 0,5 l Köstritzer
* ½ TL Brühepulver
* 20 g (4 EL) Hefeextraktpulver
* 2 TL BeefSensation
* 100 g Haferflocken
* 100 g Seitan Basis
* 300 g Aubergine
