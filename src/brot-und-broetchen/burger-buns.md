---
description: 12 Stk. á 6 cm Ø
---

# Burger Buns

## Zutaten

* 300 g Weizenmehl (Type 405)
* 300 g Dinkelmehl (Type 630)
* 1 Würfel (42 g) Hefe
* 350 g Kokos-Joghurt
* 100 ml Wasser
* 1 EL Zucker
* 1 TL Salz
* 60 g pflanzliche Butter
* 100 ml Hafersahne
* Sesamsaat zum Bestreuen

## Zubereitung

1. Alle Zutaten bis auf das Salz im Kneter zu einem geschmeidigen Teig kneten.\
   1 Stunde ruhen lassen.
2. Das Salz hinzugeben und erneut kurz durchkneten, dann Portionen von jeweils ca. 85 g in Silikonbackförmchen von 6 cm Durchmesser geben.\
   Erneut 45 Minuten gehen lassen.
3. Backofen auf 180° C vorheizen.\
   Derweil vorsichtig, sodass die aufgegangenen Brotlinge nicht wieder in sich zusammensacken, diese mit Hafersahne bestreichen. Mit Sesam bestreuen und erneut mit Hafersahne besteichen.
4. 30 Minuten backen. Dann die Brötchen aus den Silikonformen nehmen und weitere 10 Minuten auf dem Rost bei Unterhitze backen.

## Hinweis

Der Teig ist aufgrund der hohen Feuchtigkeit relativ klebrig und daher nur schwer von Hand zu kneten. Ohne Knetmaschine (KitchenAid o.Ä.) ist dieses Rezept vermutlich nur schwer machbar.\
Ohne die 100 ml Wasser lässt sich der Teig schon eher von Hand kneten, aber dann werden die Brötchen nicht so fluffig. Sie sollten dann nur 30 Minuten in den Förmchen gebacken werden und NICHT noch anschließend weitere 10 Minuten ohne die Förmchen.
