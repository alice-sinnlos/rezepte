---
description: 6 Stk. á 8 cm Ø
---

# Pfannkuchen

## Zutaten

* 200 g Weizenmehl (Type 405)
* 1 ½ TL Backpulver
* ¾ TL Salz
* 360 ml Vanille-Hafermilch
* 2 TL Tonka-Vanillezucker
