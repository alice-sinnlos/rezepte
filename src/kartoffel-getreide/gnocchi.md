---
description: ~4 Portionen
---

# Gnocchi

## Zutaten

* 500 g Kartoffeln (Solara, vorw. festkochend)
* 150 g Weizen- oder Dinkelmehl
* 25 g (1 EL, gehäuft) Kartoffelmehl
* ½ TL Salz
* Pr. Muskat
